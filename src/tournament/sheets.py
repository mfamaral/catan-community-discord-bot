import asyncio
import dataclasses
import datetime
import json
import os
import time
import traceback
import typing

import aiohttp
import jwt
import jwt.utils

GOOGLE_ACCOUNT = "catan-community-bot@catancommunity.iam.gserviceaccount.com"
SHARE_EMAIL = "mauritsvanriezen@gmail.com"
FOLDER_ID = "1DPYIzEA93wxVkZA4CioqZcUvgiRsdcts"

# SHEET_ID = "17_vLQ8Rv5bYxWvfsuhCNZTzZMUg0hXbYqJ9ts7nOa-k"
SHEET_ID = "1sZvZLrfOkc6QUv-aBY1Z92voFwtWdJMhSLC7J-7FUH8"


@dataclasses.dataclass
class PlayerScore:
    discord_name: str
    colonist_name: str
    score: typing.Optional[int]
    owns_expansion: bool


@dataclasses.dataclass
class GameScore:
    name: str
    players: typing.List[PlayerScore]


@dataclasses.dataclass
class SheetRange:
    x_start: int
    x_end: int
    y_start: int
    y_end: int
    sheet_id: typing.Optional[int] = None

    @classmethod
    def with_size(cls, x, y, width, height, sheet_id=None):
        return cls(
            x_start=x, y_start=y, x_end=x + width, y_end=y + height, sheet_id=sheet_id
        )

    def to_grid_range(self, sheet_id=None):
        print(self)
        return {
            "sheetId": sheet_id or self.sheet_id or 0,
            "startRowIndex": self.y_start,
            "endRowIndex": self.y_end,
            "startColumnIndex": self.x_start,
            "endColumnIndex": self.x_end,
        }


class Spreadsheet(object):
    def __init__(self, grid_data):
        self.sheets = {i["properties"]["title"]: i for i in grid_data["sheets"]}
        self.title = grid_data["properties"]["title"]

    def get_sheets(self) -> typing.Iterable[str]:
        return self.sheets.keys()

    def get_value(self, sheet_name: str, row: int, column: int):

        if column >= len(self.sheets[sheet_name]["data"][0]["rowData"]):
            return 0

        if len(self.sheets[sheet_name]["data"][0]["rowData"][column]) == 0:
            return None

        if row >= len(self.sheets[sheet_name]["data"][0]["rowData"][column]["values"]):
            return None

        val = self.sheets[sheet_name]["data"][0]["rowData"][column]["values"][row]
        if "effectiveValue" not in val:
            return None
        if "stringValue" in val["effectiveValue"]:
            return val["effectiveValue"]["stringValue"]
        elif "numberValue" in val["effectiveValue"]:
            return val["effectiveValue"]["numberValue"]

    def get_sheet_id(self, sheet_name):
        print(self.sheets[sheet_name]["properties"])
        return self.sheets[sheet_name]["properties"]["sheetId"]

    def get_title(self) -> str:
        return self.title

    def get_new_sheet_id(self):
        lowest_id = 4
        for sheet in self.sheets:
            if self.get_sheet_id(sheet) == lowest_id:
                lowest_id += 1
        return lowest_id


def create_cell_data(value):
    if isinstance(value, float) or isinstance(value, int):
        return {"userEnteredValue": {"numberValue": value}}
    elif isinstance(value, str):
        return {"userEnteredValue": {"stringValue": value}}
    else:
        return {}


async def get_access_token(session, config):
    content = None
    if (
        not os.path.exists("google_drive_auth_token.json")
        or time.time() - os.path.getmtime("google_drive_auth_token.json") > 60 * 30
    ):
        encoder = jwt.JWT()

        try:
            credentials = jwt.jwk_from_pem(
                config["google_private_key"]["private_key"].encode("utf-8")
            )

            data = encoder.encode(
                {
                    "iss": GOOGLE_ACCOUNT,
                    "scope": "https://www.googleapis.com/auth/drive",
                    "aud": "https://oauth2.googleapis.com/token",
                    "iat": jwt.utils.get_int_from_datetime(
                        datetime.datetime.now(datetime.timezone.utc)
                    ),
                    "exp": jwt.utils.get_int_from_datetime(
                        datetime.datetime.now(datetime.timezone.utc)
                        + datetime.timedelta(hours=1)
                    ),
                },
                credentials,
                alg="RS256",
            )

            res = await session.post(
                "https://oauth2.googleapis.com/token",
                data={
                    "grant_type": "urn:ietf:params:oauth:grant-type:jwt-bearer",
                    "assertion": data,
                },
            )

            content = await res.json()
        except ImportError:
            traceback.print_exc()

            request = await session.post(
                "https://jwt.mousetail.nl/proxy/google",
                json=config["google_private_key"],
            )

            request.raise_for_status()
            content = await request.json()

        with open("google_drive_auth_token.json", "w") as f:
            json.dump(content, f)

    if content is None:
        with open("google_drive_auth_token.json") as f:
            content = json.load(f)

    return content["access_token"]


def coords_to_cell(coords: (int, int), fixed_x=False, fixed_y=False) -> str:
    return (
        ("$" if fixed_x else "")
        + chr(coords[0] + ord("A")).upper()
        + ("$" if fixed_y else "")
        + str(coords[1] + 1)
    )


def cell_to_cooord(cell: str) -> (int, int):
    return ord(cell[0]) - ord("A"), int(cell[1:]) - 1


async def share_document(http, sheet_id, token):
    res = await http.post(
        "https://www.googleapis.com/drive/v3/files/" + sheet_id + "/permissions",
        headers={"Authorization": "Bearer " + token},
        json={
            "role": "reader",
            "type": "anyone",
        },
    )

    res.raise_for_status()

    res = await http.patch(
        "https://www.googleapis.com/drive/v3/files/" + sheet_id,
        headers={"Authorization": "Bearer " + token},
        params={"addParents": FOLDER_ID},
        json={},
    )
    print(json.dumps(await res.json(), indent=2))
    res.raise_for_status()


async def edit_document(config, name, data, sheet_id=None):
    async with aiohttp.ClientSession() as http:
        token = await get_access_token(http, config)

        if sheet_id is None:
            response = await http.post(
                "https://sheets.googleapis.com/v4/spreadsheets",
                headers={"Authorization": "Bearer " + token},
                json={
                    "properties": {
                        "title": name,
                    },
                    "sheets": {"data": [data]},
                },
            )

            if response.status >= 400:
                data = await response.json()
                print(json.dumps(data, indent=2))
                raise ValueError("There was a problem creating the document")

            data = await response.json()
            print(response.status, type(response.status), response.status >= 400)
            print(json.dumps(data, indent=2))

            sheet_id = data["spreadsheetId"]

            await share_document(http, sheet_id, token)
        else:
            res = await http.post(
                f"https://sheets.googleapis.com/v4/spreadsheets/{sheet_id}/values:batchUpdate",
                headers={"Authorization": "Bearer " + token},
                json={
                    "valueInputOption": "USER_ENTERED",
                    "data": [data],
                    "includeValuesInResponse": False,
                },
            )
            res.raise_for_status()
        return sheet_id


async def load_sheet_contents(sheet_id, config) -> Spreadsheet:
    async with aiohttp.ClientSession() as http:
        token = await get_access_token(http, config)

        sheet_response = await http.get(
            f"https://sheets.googleapis.com/v4/spreadsheets/{sheet_id}",
            params={
                "includeGridData": "true",
                "fields": "sheets/data/rowData/values/effectiveValue,sheets/properties(sheetId,title,hidden,index),properties/title",
            },
            headers={"Authorization": "Bearer " + token},
        )

        if not sheet_response.ok:
            raise ValueError(
                "Could not fetch data, please check if the drive document exists and is shared with `"
                + GOOGLE_ACCOUNT
                + "`"
            )
        sheet_data = await sheet_response.json()

        return Spreadsheet(sheet_data)


def get_message(game, completed_games, uncompleted_games):
    if game in completed_games:
        return "Yes"
    elif game in uncompleted_games:
        return 1  # Not played, used for conditional formatting
    else:
        return "No"


async def write_results_to_sheet(spreadsheet_id, sheet_id, data, sheet_names, config):
    print(sheet_names)

    async with aiohttp.ClientSession() as http:
        token = await get_access_token(http, config)

        result = await http.post(
            f"https://sheets.googleapis.com/v4/spreadsheets/{spreadsheet_id}:batchUpdate",
            headers={"Authorization": "Bearer " + token},
            json={
                "includeSpreadsheetInResponse": False,
                "requests": [
                    {
                        "updateCells": {
                            "fields": "userEnteredValue",
                            "rows": [
                                {
                                    "values": [
                                        create_cell_data(index + 1),
                                        create_cell_data(i["discord_name"]),
                                        create_cell_data(i["colonist_name"]),
                                        create_cell_data(i["wins"]),
                                        create_cell_data(i["points"]),
                                        create_cell_data(i["percent"]),
                                        create_cell_data(i["table_points"]),
                                        create_cell_data(i["point_diff"]),
                                        create_cell_data(i["avg_vp_op"]),
                                        create_cell_data(i["total_win_opponent"]),
                                        create_cell_data(i["avg_rank_opponents"]),
                                        {},  # Leave LP unchanged
                                        *(
                                            create_cell_data(
                                                get_message(
                                                    j,
                                                    i["games"],
                                                    i["uncompleted_games"],
                                                )
                                            )
                                            for j in sheet_names
                                        ),
                                    ]
                                }
                                for index, i in enumerate(data)
                            ],
                            "start": {
                                "sheetId": sheet_id,
                                "rowIndex": 2,
                                "columnIndex": 1,
                            },
                        }
                    }
                ],
            },
        )

        content = await result.json()

        if result.status == 403:
            raise ValueError(
                "Make sure the document is shared with `" + GOOGLE_ACCOUNT + "`"
            )
        elif not result.ok:
            if "error" in content:
                if "message" in content["error"]:
                    raise ValueError(content["error"]["message"])
            raise ValueError(content)


def assert_cell_condition(
    sheet,
    sheet_name,
    coords,
    assertion,
):
    t = assertion(sheet.get_value(sheet_name, coords[0], coords[1]))
    if isinstance(t, str):
        raise AssertionError(
            "Unexpected value on sheet "
            + sheet_name
            + " at "
            + coords_to_cell(coords)
            + ": "
            + t
        )


def assert_cell_equal(sheet, sheet_name, coords, value):
    return assert_cell_condition(
        sheet,
        sheet_name,
        coords,
        lambda i: None
        if isinstance(i, str) and i.lower() == value.lower()
        else f"Expected {repr(value)} got {repr(i)}",
    )


def assert_cell_string(sheet, sheet_name, coords):
    return assert_cell_condition(
        sheet,
        sheet_name,
        coords,
        lambda i: None if isinstance(i, str) else f"Expected text, got {i!r}",
    )


def assert_cell_int(sheet, sheet_name, coords):
    return assert_cell_condition(
        sheet,
        sheet_name,
        coords,
        lambda i: None if isinstance(i, int) else f"Expected a number, got {i!r}",
    )


def get_qualifier_game_info(
    document, sheet_name, nrof_players_per_game=4
) -> typing.Generator[GameScore, None, None]:
    y = 0
    done = False
    while not done:
        for x in range(4):
            coords = x * 4 + 1, y * (3 + nrof_players_per_game) + 2
            if document.get_value(sheet_name, *coords) in (None, 0):
                done = True
                break

            assert_cell_equal(
                document, sheet_name, [coords[0], coords[1]], "discord username"
            )
            assert_cell_equal(
                document, sheet_name, [coords[0] + 1, coords[1]], "colonist username"
            )
            assert_cell_equal(document, sheet_name, [coords[0] + 2, coords[1]], "vp")

            table_number = document.get_value(sheet_name, coords[0], coords[1] - 1)

            for i in range(nrof_players_per_game):
                assert_cell_string(document, sheet_name, [coords[0], coords[1] + 1 + i])
                assert_cell_string(
                    document, sheet_name, [coords[0] + 1, coords[1] + 1 + i]
                )

            yield GameScore(
                name=table_number,
                players=[
                    PlayerScore(
                        discord_name=document.get_value(
                            sheet_name, coords[0], coords[1] + i
                        ),
                        colonist_name=document.get_value(
                            sheet_name, coords[0] + 1, coords[1] + i
                        ),
                        score=document.get_value(
                            sheet_name, coords[0] + 2, coords[1] + i
                        ),
                        owns_expansion=document.get_value(
                            sheet_name, coords[0] + 3, coords[1] + i
                        )
                        == "*",
                    )
                    for i in range(1, nrof_players_per_game + 1)
                ],
            )
        y += 1


def string_range_to_grid_range(string_range, sheet_id):
    parts = string_range.split(":")
    index = [cell_to_cooord(i) for i in parts]
    return {
        "sheetId": sheet_id,
        "startRowIndex": index[0][1],
        "startColumnIndex": index[0][0],
        "endRowIndex": index[1][1] - 1,
        "endColumnIndex": index[1][0] - 1,
    }


if __name__ == "__main__":
    with open("../secret.json") as f:
        config = json.load(f)

    print(asyncio.get_event_loop())
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    # dat = asyncio.run(create_table_sheets(config, "alpha", tables))
    asyncio.run(load_sheet_contents(SHEET_ID, config))
