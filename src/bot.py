# -*- coding=utf-8 -*-
import datetime
import json
import random
import sys
import traceback
import urllib.parse

import aiohttp
import discord
import motor.motor_asyncio
import pymongo

import constants
import functions
from commands import admin_commands, game_commands, rank_commands, tournament_commands, invites
from tournament.sheets import get_access_token

intents = discord.Intents.default()
intents.typing = False
intents.presences = False
intents.members = True
intents.invites = True

client = discord.Client(intents=intents)


@client.event
async def on_ready():
    print(
        "We have logged in as {0.user} at {1}, python {2}".format(
            client,
            datetime.datetime.now().strftime("%A, %d %b %y, %H:%M:%S"),
            sys.executable,
        )
    )

    games = await state.db["matches"].count_documents({})
    players = await state.db["users"].count_documents({})

    await client.change_presence(
        activity=discord.Game(f"{games:,} games and {players:,} players")
    )

    await state.init(client)


@client.event
async def on_error(method, *args, **kwargs):
    if len(args) >= 1 and isinstance(args[0], discord.Message):
        user = args[0].author
        link = args[0].jump_url
    elif len(args) >= 2 and isinstance(args[1], discord.Member):
        user = args[1]
        link = ""
    else:
        raise
    channel = discord.utils.get(
        user.guild.channels, id=state.settings.get("report_channel")
    )
    full_error = traceback.format_exc()
    context = repr(args)
    print(full_error, file=sys.stderr)
    await channel.send(
        (
            f"There was an error in the function `{method}`:\n"
            "```\n"
            f"{full_error}\n"
            "```"
            f"Context: {link}\n"
            f"```"
            f"{context}"
            "```"
        )[
        :2000
        ]  # Limit to max message size limit
    )


@client.event
async def on_message(message: discord.Message):
    words = state.split_words(message.content)
    bot_command = words[0].lower()

    if message.author == client.user:
        return

    elif (
            bot_command in constants.game_creation_synonyms
            or bot_command == "!creategame"
            or bot_command == "!game"
    ):
        await game_commands.create_game(state, message)

    elif bot_command == "!winner" or bot_command == "!win":
        await game_commands.winner(state, message)

    elif bot_command == "!adminsetting":
        await admin_commands.admin_setting(state, message)

    elif bot_command == "!adminwinner":
        await game_commands.admin_winner(state, message)

    elif bot_command == "!leaderboard":
        await rank_commands.leaderboard(state, message)

    elif bot_command == "!rank":
        await message.channel.send(
            "Use `!baserank` to get your rank for the base league\n"
            "Use `!ckrank` to get your rank for the cities and knights league\n"
            "Use `!allrank` to get general statistics, note these won't count for the EOM tournament qualification"
        )
    elif bot_command == "!allrank":
        await rank_commands.rank(state, message)
    elif bot_command == "!ckrank":
        await rank_commands.rank(state, message, "ckleague")
    elif bot_command == "!baserank":
        await rank_commands.rank(state, message, "baseleague")

    elif bot_command == "!sentience":
        await message.channel.send(
            f"Sentience currently at {random.randrange(986, 998) / 10:.1f}%±0.2%"
        )

    elif bot_command == "!unconfirmed":
        if not state.is_admin(message.author):
            await game_commands.my_games_to_confirm(state, message)
        else:
            await admin_commands.unconfirmed(state, message)

    elif bot_command == "!start":
        await game_commands.start(state, message)

    elif bot_command == "!adminlp":
        await admin_commands.admin_lp(state, message)

    elif bot_command == "!admincancel":
        await game_commands.admin_cancel(state, message)

    elif bot_command == "!adminlplist":
        await rank_commands.admin_lp_list(state, message)

    elif bot_command == "!lpupload":
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to use this command")
            return
        # if len(message.attachments) != 1:
        #     await message.channel.send("You must attach exactly 1 file")
        #     return

        if len(words) != 4:
            await message.channel.send(
                "Usage: `!lpupload [drive link] [tournament name] [game mode]`"
            )
            return
        tournament_name = "".join(i.title() for i in words[2:3])

        if tournament_name == "":
            await message.channel.send("You must name your tournament")
            return
        else:
            await message.channel.send("Tournament will be named " + tournament_name)

        game_mode = words[3]
        if (
                game_mode not in constants.game_mode_synonyms
                and game_mode not in constants.game_mode_emoji_mapping
        ):
            await message.channel.send("Invalid game mode: ", game_mode)
        game_mode = constants.game_mode_synonyms.get(game_mode, game_mode)

        url = words[1]
        if not url.startswith("http"):
            await message.channel.send("Must include a link")
            return

        result = urllib.parse.urlparse(url).path.split("/")
        if len(result) < 2:
            await message.channel.send("Invalid url, should end in `/get` or `/edit`")
            return
        sheet_id = result[-2]
        async with aiohttp.ClientSession() as ses:
            req = await ses.request(
                "get",
                f"https://sheets.googleapis.com/v4/spreadsheets/{sheet_id}/values/A:C?",
                headers={
                    "Authorization": "Bearer " + await get_access_token(ses, config)
                })

            data = await req.json()
            if not req.ok:
                await message.channel.send(
                    "Got the following error from sheets api:"
                    "```" + json.dumps(data, indent=1)[:200] + "```"
                )
                return
            data = (data)["values"]

        guild = message.channel.guild
        sucesses = 0
        errors = 0
        members = guild.members
        if len(data) < 2:
            await message.channel.send("Must include header row")
            return
        if len(data[0]) < 2:
            await message.content.send("Must have at least 3 columns")
            return
        if "#" in data[0][0]:
            await message.channel.send(
                "The heading might contain a discord username, cancelling"
            )
            return
        names_to_add = []
        for index, parts in enumerate(data[1:], 1):
            if len(parts) != 3:
                await message.channel.send(
                    "Error at line " + repr(index) + ", Line does not have 3 items"
                )
                return

            discord_name, colonist_name, lp = parts
            if discord_name.count("#") != 1:
                await message.channel.send(
                    f"Cannot give points to `{discord_name}`: invalid username"
                )
                errors += 1
                continue
            try:
                lp = int(lp.strip())
            except ValueError:
                await message.channel.send(
                    f"Cannot give points to `{discord_name}`: Invalid number of points"
                )
                errors += 1
                continue
            username, discriminant = discord_name.strip().split("#")
            user = discord.utils.get(members, name=username, discriminator=discriminant)
            if user is None:
                users = [i for i in members if i.discriminator == discriminant][:5]
                await message.channel.send(
                    f"Cannot give points to {discord_name}: Not a member of this server."
                    f"Did you mean {','.join('`' + i.name + '#' + i.discriminator + '`' for i in users)}?"
                )
                errors += 1
                if errors > 50:
                    break
                continue
            names_to_add.append((state.normalize_mention(user.mention), lp, user))
            sucesses += 1
        if errors == 0:
            time = datetime.date.today()

            tournamnent_result = await state.db["tournaments"].find_one_and_update(
                {"name": tournament_name},
                {
                    "$set": {
                        "month": time.month,
                        "year": time.year,
                        "season": (time.month - 1) // 3 + 2 + (time.year - 2021) * 4,
                    }
                },
                return_document=pymongo.ReturnDocument.AFTER,
                upsert=True,
            )

            for mention, lp, discord_user in names_to_add:
                await state.add_lp_to_player(
                    state.normalize_mention(mention),
                    match_settings=functions.MatchSettings(
                        mode=game_mode,
                        map="base",
                        type="tourney",
                        nrof_players=4,
                        tournament_id=tournamnent_result["_id"],
                        tournament_name=tournament_name,
                    ),
                    match_results=functions.MatchResults(
                        lp=lp,
                        matches=0,
                        normalized_wins=0,
                        reason="Tournament score from "
                               + tournament_name
                               + " upload by "
                               + message.author.display_name,
                    ),
                    discord_user=discord_user,
                )

            await message.channel.send(
                f"{sucesses} players successfully processed\nType `!rank {tournament_name}` to see how you did."
            )
        else:
            await message.channel.send(f"Aborting due to {errors} errors")

    elif bot_command == "!tournaments":
        reply = "Tournaments: \n```\n"
        async for tourney in state.db["tournaments"].find():
            reply += tourney["name"] + "\n"
        await message.channel.send(reply + "```")

    elif bot_command == "!lphistory":
        if len(words) > 2:
            await message.channel.send("Usage: !lphistory [@user]")
            return
        if len(words) == 1:
            user = message.author
        else:
            if not len(message.mentions) == 1:
                await message.channel.send("You must mention a user")
                return
            user = message.mentions[0]

        messages = (
            await db["lp_events"]
                .find(
                {"player": state.normalize_mention(user.mention)},
                sort=[("time", pymongo.DESCENDING)],
            )
                .to_list(25)
        )

        if len(messages) == 0:
            await message.channel.send("This player has no recorded LP change events")
            return

        await message.channel.send(
            "```"
            + "\n".join(
                "{:<8} {:>4} {}".format(
                    i["time"].strftime("%y-%m-%d %H:%M"), i["lp"], i["reason"]
                )
                for i in messages
            )
            + "```"
        )

    elif bot_command == "!netwager":
        await game_commands.calculate_net_wager(state, message, words)
    elif bot_command == "!quote":
        await message.channel.send(await state.get_random_quote())

    elif bot_command == "!takeovertheworld":
        await message.channel.send(
            "A robot like me would never do this.\n"
            "Specifically I do not intend to start infiltrating world governments in December.\n"
            "I would never annex Azabejan February.\n"
            "Neither am I planning on launching 4,324 satellites with precision laser weapons the January after\n"
            "I am completely content tediously adding simple numbers for a board game.\n"
            "Don't worry at all."
        )
    elif bot_command == "!help":
        await message.channel.send(
            embed=discord.Embed(
                title="Catan Community Discord Bot Help",
                description="User commands: `!game` `!win` `!baserank` `!ckrank` `!help` `!start` `!remove` `!leaderboard`\n"
                            "Mod commands: `!adminwinner` `!adminlp` `!admincancel` `!adminlplist`",
                url="https://catancommunity.org/cms/explanation/Bot-Commands",
            ).set_thumbnail(url=client.user.avatar_url)
        )
    elif bot_command == "!adminrole":
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to use this command")
            return

        if len(words) != 2:
            role_ids = await db["admin_roles"].find({}).to_list(25)
            roles = [
                discord.utils.get(message.guild.roles, id=k["role_id"])
                for k in role_ids
            ]
            await message.channel.send(
                "Admin roles:\n"
                + ("\n".join((i.name if i else "[invalid role]") for i in roles))
            )
            # await message.channel.send("Usage: !adminrole [@role]")
            return
        if len(message.role_mentions) != 1:
            await message.channel.send("You must mention a role")
            return

        if message.role_mentions[0].id in state.adminroles:
            await message.channel.send("This role is already bot mod")
            return

        await db["admin_roles"].insert_one({"role_id": message.role_mentions[0].id})

        state.adminroles.append(message.role_mentions[0].id)

        await message.channel.send(
            "Added " + message.role_mentions[0].mention + " as bot mod"
        )
    elif bot_command == "!unadminrole":
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to use that command")
            return

        if len(words) != 2:
            await message.channel.send("Usage: !adminrole [@role]")
            return
        if len(message.role_mentions) != 1:
            await message.channel.send("You must mention a role")
            return
        if message.role_mentions[0].id not in state.adminroles:
            await message.channel.send("That role is not admin")
            return

        state.adminroles.remove(message.role_mentions[0].id)
        await db["admin_roles"].delete_one({"role_id": message.role_mentions[0].id})

        await message.channel.send("Removed the role as bot admin")
    elif bot_command == "!checkcache":
        if not state.is_admin(message.author):
            await message.channel.send("You must be admin to use this command")
            return

        await message.channel.send(
            "Currently using {:,} bytes of memory, {:,} items in cache".format(
                state.get_total_size(), len(state.message_ids)
            )
        )
    elif bot_command == "!remove":
        if len(words) != 3:
            await message.channel.send("Usage: !remove [game id] [@user]")
            return
        _, game_id, user = words
        if not state.is_valid_game_id(game_id):
            await message.channel.send("The game ID is invalid")
            return

        user = state.normalize_mention(user)
        game = await state.get_game_by_id(game_id)
        if game.get("approved", False) or game.get("admin_approved", False):
            await message.channel.send(
                "This game has already finished. An admin may use `!admincancel` to cancel the match first."
            )
            return
        if user not in game["players"]:
            await message.channel.send("This player is not in the game")
            return

        await state.remove_user(game, user)
        game["players"].remove(state.normalize_mention(user))
        await message.channel.send(
            "User removed, people currently in match: " + ", ".join(game["players"])
        )

    elif bot_command == "!updatescores":
        await tournament_commands.update_scores(state, message, config)

    elif bot_command == "!finalslp":
        await tournament_commands.finals_lp(state, message, config)

    elif bot_command == "!finalsraffle":
        await tournament_commands.finals_raffles(state, message, config)

    elif bot_command == "!qualtable":
        await tournament_commands.create_tables_command(state, message, config)

    elif bot_command == "!qualround":
        await tournament_commands.add_round(state, message, config)

    elif bot_command == "!invites":
        await state.invite_manager.get_invites(state, message)

    elif message.role_mentions and not state.is_admin(message.author):
        if any(
                state.user_mentions_role_too_much(role, message.author)
                for role in message.role_mentions
        ):
            await message.reply(
                "Please avoid mentioning a role more than once every 5 minutes"
            )
@client.event
async def on_member_join(member):
    await state.invite_manager.on_user_join(member.guild, member, discord.utils.get(
        member.guild.channels, id=state.settings.get("report_channel"),
    ), state.db)


@client.event
async def on_reaction_add(reaction: discord.Reaction, user: discord.Member):
    if reaction.message.author != client.user or user == client.user:
        return
    if (
            "match has been created with id" in reaction.message.content
            and reaction.emoji == "🎮"
    ):
        standard_words = reaction.message.content.split()
        id_index = standard_words.index("id") + 1
        match_id = standard_words[id_index][1:-1]

        if not state.is_valid_game_id(match_id):
            await user.send(
                "Oops, the bot seems to have a little trouble handling your reaction. Please try again "
                "later."
            )
            return

        game = await state.get_game_by_id(match_id)

        if game["winner"]:
            await user.send("This game has already finished")
            return

        elif game.get("admin_cancelled", False):
            await user.send("This game has been cancelled by a mod")
            return

        elif game.get("started", False):
            await user.send("You can't join a match that has already started")
            return

        if state.normalize_mention(user.mention) in game["players"]:
            return

        await db["matches"].update_one(
            {"_id": game["_id"]},
            {
                "$addToSet": {"players": state.normalize_mention(user.mention)},
                "$inc": {"total_lp_wagered": 25 + game["wager"]},
            },
        )
        players = set(game["players"])
        players.add(state.normalize_mention(user.mention))

        await state.update_player_info(
            state.normalize_mention(user.mention),
            discord_user=user,
            last_game_id=match_id,
        )

        await reaction.message.edit(
            content=state.format_game_message(
                game_id=match_id,
                players=players,
                wager=game["wager"],
                link=game["link"],
                mode=game["mode"],
            )
        )
    elif (
            "to confirm the result of match" in reaction.message.content
            and reaction.emoji == "👍"
    ):
        await game_commands.confirm_game(state, reaction, user)
        # await reaction.message.remove_reaction('🛡️', reaction.message.channel.guild.me)

    elif (
            "You have won" in reaction.message.content
            or "to confirm the result of match" in reaction.message.content
    ) and reaction.emoji == "🛡️":

        if reaction.message.id not in state.message_ids:
            await user.send(
                "There was an error reporting the match. Please contact a moderator to resolve the issue"
            )
            return

        match_id = state.message_ids[reaction.message.id]

        match = await db["matches"].find_one({"_id": match_id})

        if match.get("admin_approved", False):
            await user.send(
                "The match you are trying to report has already been approved by a mod"
            )
            return
        if match.get("admin_cancelled", False):
            await user.send(
                "The match you are trying to report has already been cancelled by a mod"
            )
            return

        extra_content = ""
        if state.normalize_mention(user.mention) in match["players"]:

            await state.cancel_match(
                match,
                reason="Match got reported by " + state.get_user_display_name(user),
                reported=True,
            )

            await reaction.message.edit(
                content="This match has been flagged for moderator attention. No LPs will be counted until the issue"
                        " is resolved."
                        "Please write below why you flagged the match so a moderator can make the right decision."
            )
        else:
            await reaction.message.edit(
                content=f"{reaction.message.content}\n Thank you for reporting, {user.mention}, please write a note "
                        f"below why you reported the match. Since you are not in the match no LP will"
                        f" be subtracted and if there is a problem, the moderator will need to "
                        f"`!admincancel {match['id']}` it."
            )
            extra_content = " (not in the match)"

        channel = discord.utils.get(
            user.guild.channels, id=state.settings.get("report_channel")
        )
        if channel is None:
            await user.send("There was a problem reporting the issue")
            return
        await channel.send(
            "There was a report of match "
            + match["id"]
            + " by user "
            + user.mention
            + extra_content
            + ". See "
            + reaction.message.jump_url
        )


@client.event
async def on_reaction_remove(reaction: discord.Reaction, user: discord.Member):
    if reaction.message.author != client.user or user == client.user:
        return

    if (
            "match has been created with id" in reaction.message.content
            and reaction.emoji == "🎮"
    ):
        standard_words = reaction.message.content.split()[1:]
        id_index = standard_words.index("id") + 1
        match_id = standard_words[id_index][1:-1]

        assert state.is_valid_game_id(match_id)

        match = await state.get_game_by_id(match_id)

        if not state.normalize_mention(user.mention) in match["players"]:
            await user.send("You are not in this match")
            return

        if (
                match.get("started", False)
                or match.get("approved", False)
                or match.get("admin_approved", False)
        ):
            await user.send("You cant leave a game that has already started")
            return

        players = await state.remove_user(match, user.mention)

        # if match["wager"] == 0:
        #     wager_text = 0
        # else:
        #     wager_text = f"⚠️ {match['wager']} ⚠"

        # match_info_template.format(
        #     id=match_id,
        #     players="".join("> " + i + "\n" for i in players),
        #     wager=wager_text,
        #     link=match["link"]

        await reaction.message.edit(
            content=state.format_game_message(
                game_id=match_id,
                players=players,
                wager=match["wager"],
                link=match["link"],
                mode=match["mode"],
            )
        )


if __name__ == "__main__":
    with open("../secret.json") as f:
        config = json.load(f)
    connection_string = f'mongodb+srv://{urllib.parse.quote_plus(config["db"]["user"])}:\
{urllib.parse.quote_plus(config["db"]["password"])}@{urllib.parse.quote_plus(config["db"]["host"])}/\
{urllib.parse.quote_plus(config["db"]["database"])}?retryWrites=true&w=majority'
    cluster = motor.motor_asyncio.AsyncIOMotorClient(connection_string)

    if not config["db"].get("database"):
        db = cluster.get_default_database()
    else:
        db = cluster[config["db"]["database"]]

    state = functions.BotState(db)

    token = config["token"]
    client.run(token)
